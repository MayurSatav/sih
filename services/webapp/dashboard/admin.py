from django.contrib import admin
from .models import Student, Tweet, Profile, School

admin.site.register(Student)
admin.site.register(Tweet)
admin.site.register(Profile)
admin.site.register(School)


